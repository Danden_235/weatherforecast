package com.WeatherForecast.weather;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesHolder {

    private static Properties PROPERTIES = new Properties();

    static {
        Thread currentThread = Thread.currentThread();
        ClassLoader contextClassLoader = currentThread.getContextClassLoader();
        InputStream propertiesStream = contextClassLoader.getResourceAsStream("properties/application.properties");
        if (propertiesStream != null) {
            try {
                PROPERTIES.load(propertiesStream);
            } catch (IOException e) {
                System.out.println("Wrong properties file");
            }
        } else {
            System.out.println("File not found");
        }
    }

    public static String getProperty(String key) {
        return PROPERTIES.getProperty(key);
    }

}
