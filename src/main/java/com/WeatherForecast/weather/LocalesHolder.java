package com.WeatherForecast.weather;

import java.util.Locale;

public class LocalesHolder {

    private static LocalesHolder INSTANCE;

    public static Locale[] supportedLocales = {
            Locale.ENGLISH,
            new Locale("ru"),
            new Locale("uk")
    };

    private static Locale currentLocale;

    private LocalesHolder(){
    }

    public static LocalesHolder getInstance() {
        if (INSTANCE == null){
            INSTANCE = new LocalesHolder();
        }
        return INSTANCE;
    }

    public void setCurrentLocale(Locale locale){
        currentLocale = locale;
    }

    public Locale getCurrentLocale(){
        return currentLocale;
    }

}
