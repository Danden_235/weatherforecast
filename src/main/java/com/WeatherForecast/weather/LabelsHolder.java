package com.WeatherForecast.weather;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;

public class LabelsHolder {

    private static ResourceBundle mLabels;

    public static String getLabel(String key) {
        Locale currentLocale = LocalesHolder.getInstance().getCurrentLocale();
        String label;

        if (mLabels == null) {
            mLabels = ResourceBundle.getBundle(
                    "Messages", currentLocale);
        }

        label = checkForCyrillic(mLabels.getString(key));

        return label;
    }

    public static String getLabel(String key, Locale language) {
        return checkForCyrillic(ResourceBundle.getBundle("Messages", language).getString(key));
    }

    private static String checkForCyrillic(String label) {

        try {
            return new String(label.getBytes("Cp1252"), "Cp1251");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported encoding");
        }

        return label;
    }
}
