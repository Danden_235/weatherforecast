package com.WeatherForecast.weather;

import com.WeatherForecast.util.Display;
import com.WeatherForecast.util.Input;

public class Menu {

    private LocalesHolder localesHolder = LocalesHolder.getInstance();
    private ForecastBuilder mExecutor = ForecastBuilder.getInstance();

    public void runMenu() {

        Display.show(LabelsHolder.getLabel("MenuMSG1", LocalesHolder.supportedLocales[0]));
        setLanguage();

        Display.show(LabelsHolder.getLabel("MenuMSG2"));
        setForecastType();

        Display.show(LabelsHolder.getLabel("MenuMSG3"));
        setCityName();

        mExecutor.buildForecast();
    }

    private void setLanguage() {
        boolean isInputRight;
        String userInput = null;

        do {
            userInput = Input.getUserInput();
            if (PropertiesHolder.getProperty("langEN").equals(userInput)) {
                localesHolder.setCurrentLocale(LocalesHolder.supportedLocales[0]);
                isInputRight = true;
            } else if (PropertiesHolder.getProperty("langRU").equals(userInput)) {
                localesHolder.setCurrentLocale(LocalesHolder.supportedLocales[1]);
                isInputRight = true;
            } else if (PropertiesHolder.getProperty("langUK").equals(userInput)) {
                localesHolder.setCurrentLocale(LocalesHolder.supportedLocales[2]);
                isInputRight = true;
            } else {
                System.out.println(LabelsHolder.getLabel("WrongLangError", LocalesHolder.supportedLocales[0]));
                isInputRight = false;
            }

        } while (!isInputRight);
    }

    private void setCityName() {

        String cityName = null;
        boolean isValid = false;

        while (!isValid) {
            cityName = Input.getUserInput();
            isValid = mExecutor.isValidCityName(cityName);
        }

        mExecutor.setCityName(cityName);
    }

    private void setForecastType() {
        boolean isInputRight;

        do {
            String userInput = Input.getUserInput();
            if (PropertiesHolder.getProperty("PositiveAnswer").equals(userInput)) {
                mExecutor.setForecastType(ForecastBuilder.FORECAST);
                isInputRight = true;
            } else if (PropertiesHolder.getProperty("NegativeAnswer").equals(userInput)) {
                mExecutor.setForecastType(ForecastBuilder.CURRENT);
                isInputRight = true;
            } else {
                System.out.println(LabelsHolder.getLabel("WrongInputError"));
                isInputRight = false;
            }
        } while (!isInputRight);
    }
}
