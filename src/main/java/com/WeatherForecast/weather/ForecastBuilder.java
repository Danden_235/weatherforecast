package com.WeatherForecast.weather;

import com.WeatherForecast.models.current.CurrentWeatherObject;
import com.WeatherForecast.models.forecast.ForecastWeatherObject;
import com.WeatherForecast.util.Display;
import com.mashape.unirest.http.HttpResponse;

public class ForecastBuilder {

    private static final int RESPONSE_STATUS_OK = 200;

    private static ForecastBuilder INSTANCE;

    public static boolean FORECAST = true;
    public static boolean CURRENT = false;

    private static String cityName;
    private static boolean forecastType;

    private Object weatherData;

    Requester requester = new Requester();
    Display display = new Display();

    private ForecastBuilder() {
    }

    public static ForecastBuilder getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ForecastBuilder();
        }

        return INSTANCE;
    }

    public void buildForecast() {

        if (forecastType == CURRENT) {
            //Request
            HttpResponse<CurrentWeatherObject> response = requester.requestCurrent();
            //Parse
            weatherData = response.getBody();
        } else {
            //Request
            HttpResponse<ForecastWeatherObject> response = requester.requestForecast();
            //Parse
            weatherData = response.getBody();
        }
        display.showWeather(weatherData);
    }

    public boolean isValidCityName(String cityName) {


        if (requester.requestValidation(cityName) == RESPONSE_STATUS_OK) {
            return true;
        } else {
            System.out.println(LabelsHolder.getLabel("UnknownCityError"));
            return false;
        }

    }

    public void setCityName(String name) {
        cityName = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setForecastType(boolean type) {
        forecastType = type;
    }

}
