package com.WeatherForecast.weather;


import com.WeatherForecast.models.current.CurrentWeatherObject;
import com.WeatherForecast.models.forecast.ForecastWeatherObject;
import com.WeatherForecast.util.ParametersBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;

public class Requester {
    private ParametersBuilder paramsBuilder = new ParametersBuilder();

    static {
        Unirest.setObjectMapper(new com.mashape.unirest.http.ObjectMapper() {
            private ObjectMapper jacksonObjectMapper
                    = new ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public HttpResponse<CurrentWeatherObject> requestCurrent() {

        HttpResponse<CurrentWeatherObject> weatherResponse = null;

        try {
            weatherResponse = Unirest.get(PropertiesHolder.getProperty("CurrentWeatherURL"))
                    .queryString(paramsBuilder.getParams())
                    .asObject(CurrentWeatherObject.class);

        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return weatherResponse;
    }

    public HttpResponse<ForecastWeatherObject> requestForecast() {

        HttpResponse<ForecastWeatherObject> weatherResponse = null;

        try {
            weatherResponse = Unirest.get(PropertiesHolder.getProperty("ForecastURL"))
                    .queryString(paramsBuilder.getParams())
                    .asObject(ForecastWeatherObject.class);

        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return weatherResponse;
    }

    public int requestValidation(String cityName) {

        int response = -1;

        try {
            response = Unirest.get(PropertiesHolder.getProperty("ForecastURL"))
                    .queryString(paramsBuilder.getValidationParams(cityName))
                    .asString()
                    .getStatus();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return response;
    }

}
