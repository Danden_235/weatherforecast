package com.WeatherForecast.util;


import com.WeatherForecast.weather.ForecastBuilder;
import com.WeatherForecast.weather.LocalesHolder;
import com.WeatherForecast.weather.PropertiesHolder;

import java.util.LinkedHashMap;
import java.util.Map;

public class ParametersBuilder {

    public Map<String, Object> getParams() {

        ForecastBuilder forecastBuilder = ForecastBuilder.getInstance();

        Map<String, Object> params = new LinkedHashMap<>();

        params.put(PropertiesHolder.getProperty("CityKey"), forecastBuilder.getCityName());
        params.put(PropertiesHolder.getProperty("IDKey"), PropertiesHolder.getProperty("IDValue"));
        params.put(PropertiesHolder.getProperty("LangKey"), getLanguageParam());
        params.put(PropertiesHolder.getProperty("UnitKey"), PropertiesHolder.getProperty("UnitValue"));

        return params;
    }

    public Map<String, Object> getValidationParams(String cityName) {

        Map<String, Object> params = new LinkedHashMap<>();

        params.put(PropertiesHolder.getProperty("CityKey"), cityName);
        params.put(PropertiesHolder.getProperty("IDKey"), PropertiesHolder.getProperty("IDValue"));
        params.put(PropertiesHolder.getProperty("LangKey"), getLanguageParam());
        params.put(PropertiesHolder.getProperty("UnitKey"), PropertiesHolder.getProperty("UnitValue"));

        return params;
    }

    private String getLanguageParam(){
        LocalesHolder holder = LocalesHolder.getInstance();
        if (holder.getCurrentLocale() == LocalesHolder.supportedLocales[2]){
            return PropertiesHolder.getProperty("paramLangUK");
        } else {
            return holder.getCurrentLocale().getLanguage();
        }
    }
}
