package com.WeatherForecast.util;


import com.WeatherForecast.models.current.CurrentWeatherObject;
import com.WeatherForecast.models.forecast.ForecastWeatherObject;
import com.WeatherForecast.models.forecast.List;
import com.WeatherForecast.weather.LabelsHolder;
import com.WeatherForecast.weather.PropertiesHolder;

public class OutputBuilder {

    public String buildCWMessage(CurrentWeatherObject weatherData) {
        String message;

        int sunrise = weatherData.getSys().getSunrise();
        int sunset = weatherData.getSys().getSunset();

        message = String.format(LabelsHolder.getLabel("DisplayMSGCurrent"),
                weatherData.getName(),
                weatherData.getSys().getCountry(),
                weatherData.getWeather().get(0).getMain(),
                weatherData.getWeather().get(0).getDescription(),
                weatherData.getWind().getSpeed(),
                weatherData.getClouds().getAll(),
                Util.convertUnixTimeToString(sunset, PropertiesHolder.getProperty("CWTimePattern")),
                Util.convertUnixTimeToString(sunrise, PropertiesHolder.getProperty("CWTimePattern")),
                weatherData.getMain().getHumidity(),
                weatherData.getMain().getPressure(),
                weatherData.getMain().getTemp(),
                weatherData.getMain().getTempMin(),
                weatherData.getMain().getTempMax());

        return message;
    }

    public void buildFCMessage(ForecastWeatherObject weatherData) {
        String message;
        java.util.List<List> dataList;
        dataList = weatherData.getList();

        for (int i = 0; i < weatherData.getList().size(); i++) {
            List fragment = dataList.get(i);

            message = String.format(LabelsHolder.getLabel("DisplayMSGForecast"),
                    Util.convertUnixTimeToString(fragment.getDt(), PropertiesHolder.getProperty("FCTimePattern")),
                    fragment.getWeather().get(0).getMain(),
                    fragment.getWeather().get(0).getDescription(),
                    fragment.getMain().getTemp(),
                    fragment.getMain().getPressure(),
                    fragment.getMain().getHumidity(),
                    fragment.getWind().getSpeed(),
                    fragment.getClouds().getAll());

            System.out.println(message);
        }
    }
}
