package com.WeatherForecast.util;

import com.WeatherForecast.weather.LocalesHolder;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Util {

    public static String convertUnixTimeToString(int unixValue, String pattern) {

        LocalesHolder holder = LocalesHolder.getInstance();

        Date date = new Date(unixValue * 1000L);
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, holder.getCurrentLocale());

        return dateFormat.format(date);
    }

}
