package com.WeatherForecast.util;

import com.WeatherForecast.models.current.CurrentWeatherObject;
import com.WeatherForecast.models.forecast.ForecastWeatherObject;

public class Display {

    private static OutputBuilder builder = new OutputBuilder();

    public static void show(String text) {
        System.out.println(text);
    }

    public static void show(int number) {
        System.out.println(number);
    }

    public void showWeather(Object weatherData) {
        if (weatherData.getClass() == CurrentWeatherObject.class) {
            showCurrentWeather(weatherData);
        } else {
            showForecastWeather(weatherData);
        }
    }

    private void showCurrentWeather(Object weatherData) {
        show(builder.buildCWMessage((CurrentWeatherObject) weatherData));
    }


    private void showForecastWeather(Object weatherData) {
        builder.buildFCMessage((ForecastWeatherObject) weatherData);
    }

}
