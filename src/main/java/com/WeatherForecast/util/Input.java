package com.WeatherForecast.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Input {

    public static String getUserInput() {
        String s = null;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            s = reader.readLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return s;
    }
}
